/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.service;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.HueGoalApp;
import ca.mudar.huegoallight.ui.listener.SyncBusEvents;
import ca.mudar.huegoallight.utils.ConnectionUtils;
import ca.mudar.huegoallight.utils.GcmUtils;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class TopicsUpdateService extends JobIntentService {
    private static final String TAG = makeLogTag("TopicsUpdateService");

    public static void enqueueWork(Context context, String topic, boolean subscribe) {
        final Bundle extras = new Bundle();
        extras.putString(Const.BundleKeys.TEAM_SLUG, topic);
        extras.putBoolean(Const.BundleKeys.TOPIC_SUBSCRIBE, subscribe);

        final Intent intent = new Intent(context, TopicsUpdateService.class);
        intent.putExtras(extras);

        enqueueWork(context, TopicsUpdateService.class, Const.APP_JOB_ID, intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (!ConnectionUtils.hasConnection(getApplicationContext())) {
            return;
        }

        final String team = intent.getStringExtra(Const.BundleKeys.TEAM_SLUG);
        final boolean subscribe = intent.getBooleanExtra(Const.BundleKeys.TOPIC_SUBSCRIBE, false);
        if (TextUtils.isEmpty(team)) {
            return;
        }

        try {
            if (subscribe) {
                GcmUtils.subscribeTeam(team);
            } else {
                GcmUtils.unsubscribeTeam(team);
            }
        } catch (Exception e) {
            e.printStackTrace();
            HueGoalApp.getSyncBus().post(new SyncBusEvents.FcmTopicsError(team, e));
        }

        HueGoalApp.getSyncBus().post(new SyncBusEvents.FcmTopicsUpdated());
    }
}
