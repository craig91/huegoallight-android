/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.service;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;
import android.os.SystemClock;
import android.text.format.DateUtils;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.squareup.otto.Subscribe;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.app.AlarmManagerCompat;
import androidx.core.app.NotificationCompat;
import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.Const.RequestCodes;
import ca.mudar.huegoallight.HueGoalApp;
import ca.mudar.huegoallight.data.DatabaseHelper;
import ca.mudar.huegoallight.data.GcmDataWrapper;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.model.GameSettings;
import ca.mudar.huegoallight.model.NhlTeam;
import ca.mudar.huegoallight.receiver.DelayedBlinkReceiver;
import ca.mudar.huegoallight.ui.listener.SyncBusEvents;
import ca.mudar.huegoallight.ui.listener.SyncBusListener;
import ca.mudar.huegoallight.utils.GcmUtils;
import ca.mudar.huegoallight.utils.HueBlinkHelper;
import ca.mudar.huegoallight.utils.HueBridgeConnectionManager;
import ca.mudar.huegoallight.utils.LogUtils;
import ca.mudar.huegoallight.utils.NotificationUtils;

import static ca.mudar.huegoallight.utils.LogUtils.LOGV;
import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class MessagingListenerService extends FirebaseMessagingService implements
        SyncBusListener {
    private static final String TAG = makeLogTag("MessagingListenerService");
    private static final String MESSAGE_DATA_KEY = "dataWrapper";

    @NonNull
    private HueBridgeConnectionManager mHueBridgeConnectionManager;

    @Override
    public void onCreate() {
        super.onCreate();

        mHueBridgeConnectionManager = new HueBridgeConnectionManager();
    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);

        try {
            // Subscribe to global notifications
            GcmUtils.subscribeDefaultTopics();
        } catch (Exception e) {
            HueGoalApp.getSyncBus().post(new SyncBusEvents.FcmRegistrationError(e));
            e.printStackTrace();
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        mHueBridgeConnectionManager.onStart();
        registerSyncBus();

        try {
            if (mHueBridgeConnectionManager.isHueBridgeConnected(getApplicationContext())) {
                final String dataWrapper = remoteMessage.getData().get(MESSAGE_DATA_KEY);
                final GcmDataWrapper gcmDataWrapper = GcmDataWrapper.fromJson(dataWrapper);

                final NhlTeam team = gcmDataWrapper.getNhlTeam();
                final String gameId = gcmDataWrapper.getGame();
                final List<Integer> colors = gcmDataWrapper.getColors();
                final long sentAt = gcmDataWrapper.getTimestamp();

                final GameSettings gameSettings = DatabaseHelper.getGameSettings(getApplicationContext(), gameId);

                if (gameSettings.isMuted()) {
                    // Skip muted game
                    LOGV(TAG, "Game is on mute");
                    return;
                }

                final HuePrefs prefs = HuePrefs.getInstance(getApplicationContext());

                final boolean hasExpired = Math.abs(System.currentTimeMillis() - sentAt) > Const.NOTIFY_MAX_DELAY;
                // Message is silent when ignoring opponents's goals, or late notification
                final boolean isSilent = prefs.isCurrentlyQuietHours() ||
                        prefs.ignoreOpponentTeam(team.getSlug()) ||
                        hasExpired;

                // Display notification
                sendNotification(team, isSilent, gameSettings.hasDelay(), prefs);

                if (!isSilent) {
                    if (gameSettings.hasDelay()) {
                        // Blink after delay
                        blinkAfterDelay(dataWrapper, gameSettings.getDelay());
                    } else {
                        // startBlinking()
                        new HueBlinkHelper(getApplicationContext(), colors, team.getSlug())
                                .startBlinking();
                    }
                } else {
                    LOGV(TAG, "Silent notification");
                }
            }
        } catch (Exception e) {
            LogUtils.REMOTE_LOG(e);
        }

        release();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        release();
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void registerSyncBus() {
        try {
            HueGoalApp.getSyncBus().register(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void unregisterSyncBus() {
        try {
            HueGoalApp.getSyncBus().unregister(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param team
     * @param isSilent
     * @param hasDelay
     * @param prefs
     */
    private void sendNotification(NhlTeam team, boolean isSilent, boolean hasDelay, HuePrefs prefs) {
        if (!prefs.hasNotifications() || !NotificationUtils.areNotificationsEnabled(getApplicationContext())) {
            return;
        }

        final NotificationCompat.Builder builder = NotificationUtils.getNotificationBuilder(
                getApplicationContext(), team);
        builder.setUsesChronometer(hasDelay);

        // Skip ringtone/vibration for silent games and for delayed games (to avoid spoiler)
        // For Oreo and later, user can adjust notification channels settings
        if (!Const.SUPPORTS_OREO && !isSilent && !hasDelay) {
            // FIXME Oreo will show notification (and sound) ignoring user-defined delay
            final boolean hasVibration = prefs.hasVibration();
            final Uri ringtone = prefs.getRingtonePath();

            if (hasVibration && ringtone != null) {
                builder.setDefaults(Notification.DEFAULT_VIBRATE)
                        .setSound(ringtone, AudioManager.STREAM_NOTIFICATION);
            } else if (hasVibration) {
                builder.setDefaults(Notification.DEFAULT_VIBRATE);
            } else if (ringtone != null) {
                builder.setSound(ringtone);
            }
        }

        final NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(RequestCodes.NOTIFY_ID, builder.build());
    }

    @Subscribe
    public void onBridgeConnected(SyncBusEvents.BridgeConnected event) {
        mHueBridgeConnectionManager.onBridgeConnected();
    }

    @Subscribe
    public void onBridgeConnectionResumed(SyncBusEvents.BridgeConnectionResumed event) {
        mHueBridgeConnectionManager.onBridgeConnectionResumed();
    }

    @Subscribe
    public void onCacheUpdated(SyncBusEvents.HueCacheUpdated event) {
        mHueBridgeConnectionManager.onCacheUpdated();
    }

    private void blinkAfterDelay(String dataWrapper, int seconds) {
        final Context context = getApplicationContext();

        final int uniqueID = (int) System.currentTimeMillis();
        final PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                uniqueID,
                DelayedBlinkReceiver.newIntent(context, dataWrapper),
                PendingIntent.FLAG_UPDATE_CURRENT);

        final long triggerAt = SystemClock.elapsedRealtime() + (seconds * DateUtils.SECOND_IN_MILLIS);
        AlarmManagerCompat.setExactAndAllowWhileIdle(
                (AlarmManager) context.getSystemService(Context.ALARM_SERVICE),
                AlarmManager.ELAPSED_REALTIME_WAKEUP,
                triggerAt,
                pendingIntent);
    }

    /**
     * Manual cleanup because onDestroy() is not called immediately for FirebaseMessagingService
     */
    private void release() {
        mHueBridgeConnectionManager.onStop();
        unregisterSyncBus();
    }
}
