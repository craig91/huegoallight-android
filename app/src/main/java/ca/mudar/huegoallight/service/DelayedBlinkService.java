/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.service;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.squareup.otto.Subscribe;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.Const.BundleKeys;
import ca.mudar.huegoallight.HueGoalApp;
import ca.mudar.huegoallight.data.GcmDataWrapper;
import ca.mudar.huegoallight.model.NhlTeam;
import ca.mudar.huegoallight.ui.listener.SyncBusEvents;
import ca.mudar.huegoallight.ui.listener.SyncBusListener;
import ca.mudar.huegoallight.utils.HueBlinkHelper;
import ca.mudar.huegoallight.utils.HueBridgeConnectionManager;
import ca.mudar.huegoallight.utils.LogUtils;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class DelayedBlinkService extends JobIntentService implements
        SyncBusListener {
    private static final String TAG = makeLogTag("DelayedBlinkService");

    @NonNull
    private HueBridgeConnectionManager mHueBridgeConnectionManager;

    public static void enqueueWork(Context context, String dataWrapper) {
        final Intent intent = new Intent(context, DelayedBlinkService.class);

        final Bundle extras = new Bundle();
        extras.putString(BundleKeys.DATA_WRAPPER, dataWrapper);
        intent.putExtras(extras);

        enqueueWork(context, DelayedBlinkService.class, Const.APP_JOB_ID, intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mHueBridgeConnectionManager = new HueBridgeConnectionManager();
        registerSyncBus();
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        mHueBridgeConnectionManager.onStart();

        try {
            if (mHueBridgeConnectionManager.isHueBridgeConnected(getApplicationContext())) {
                final String dataWrapper = intent.getStringExtra(BundleKeys.DATA_WRAPPER);
                final GcmDataWrapper gcmDataWrapper = GcmDataWrapper.fromJson(dataWrapper);

                final NhlTeam team = gcmDataWrapper.getNhlTeam();
                final List<Integer> colors = gcmDataWrapper.getColors();

                new HueBlinkHelper(getApplicationContext(), colors, team.getSlug()).startBlinking();
            }
        } catch (Exception e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mHueBridgeConnectionManager.onStop();
        unregisterSyncBus();
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void registerSyncBus() {
        try {
            HueGoalApp.getSyncBus().register(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void unregisterSyncBus() {
        try {
            HueGoalApp.getSyncBus().unregister(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    @Subscribe
    public void onBridgeConnected(SyncBusEvents.BridgeConnected event) {
        mHueBridgeConnectionManager.onBridgeConnected();
    }

    @Subscribe
    public void onBridgeConnectionResumed(SyncBusEvents.BridgeConnectionResumed event) {
        mHueBridgeConnectionManager.onBridgeConnectionResumed();
    }

    @Subscribe
    public void onCacheUpdated(SyncBusEvents.HueCacheUpdated event) {
        mHueBridgeConnectionManager.onCacheUpdated();
    }
}
