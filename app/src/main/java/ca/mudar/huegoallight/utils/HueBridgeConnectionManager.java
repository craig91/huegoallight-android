/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.SystemClock;
import android.text.format.DateUtils;

import com.philips.lighting.hue.sdk.PHHueSDK;

import androidx.annotation.WorkerThread;
import ca.mudar.huegoallight.ui.listener.HueSDKListener;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class HueBridgeConnectionManager {
    private static final String TAG = makeLogTag("HueBridgeConnectionManager");
    private static final int MAX_RETRIES = 12; // Retry Bridge connection 12 sec (default heartbeat is 10 sec)

    private boolean mIsBridgeConnected;
    private boolean mIsCacheUpdated;

    public HueBridgeConnectionManager() {
        onStart();
    }

    public void onStart() {
        this.mIsBridgeConnected = false;
        this.mIsCacheUpdated = false;
    }

    public void onStop() {
        HueBridgeUtils.disconnectBridgeIfNecessary(PHHueSDK.getInstance());
    }

    public void onBridgeConnected() {
        mIsBridgeConnected = true;
    }

    public void onBridgeConnectionResumed() {
        mIsBridgeConnected = true;
    }

    public void onCacheUpdated() {
        mIsCacheUpdated = true;
        mIsBridgeConnected = true; // updated cache means connected bridge
    }

    /**
     * /**
     * Check if the Bridge is connected. Has a wait loop, to handle the bridge re-connection delay
     *
     * @param context
     * @return true if bridge is connected
     */
    @WorkerThread
    public boolean isHueBridgeConnected(Context context) {
        final PHHueSDK hueSDK = PHHueSDK.getInstance();
        final HueSDKListener listener = HueBridgeUtils
                .initializeHueBridge(new ContextWrapper(context), hueSDK);
        listener.setFastHeartBeatInterval();

        int retry = 0;
        while (retry < MAX_RETRIES && !mIsCacheUpdated) {
            SystemClock.sleep(DateUtils.SECOND_IN_MILLIS);
            retry++;
        }
        return mIsBridgeConnected;
    }
}
