/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.utils;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.AudioAttributes;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.model.NhlTeam;
import ca.mudar.huegoallight.ui.activity.MainActivity;

public class NotificationUtils {

    public static NotificationCompat.Builder getNotificationBuilder(Context context, @NonNull NhlTeam team) {

        final Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        final PendingIntent pendingIntent = PendingIntent.getActivity(context,
                0,
                intent,
                PendingIntent.FLAG_ONE_SHOT);

        final Resources res = context.getResources();
        final String contentTitle = res.getString(R.string.notify_goal_title);
        final String contentText = res.getString(R.string.notify_goal_text,
                res.getString(team.getTeamName()));

        final String channelId = setupNotificationChannelIfNecessary(context, team);
        return new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(team.getTeamNotifyIcon())
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setColor(team.getColor())
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);
    }

    @TargetApi(Build.VERSION_CODES.O)
    public static String setupNotificationChannelIfNecessary(Context context, @NonNull NhlTeam team) {
        if (!Const.SUPPORTS_OREO) {
            return Const.NOTIFY_DEFAULT_CHANNEL_ID;
        }

        final NotificationChannel channel = getTeamNotificationChannel(context, team);
        channel.setSound(Settings.System.DEFAULT_NOTIFICATION_URI, new AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build());
        channel.enableLights(true);
        channel.enableVibration(true);
        channel.setLightColor(team.getColor());
        channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

        final NotificationManager notificationManager = context
                .getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);

        return channel.getId();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private static NotificationChannel getTeamNotificationChannel(Context context, @NonNull NhlTeam team) {
        final boolean isFollowingTeam = HuePrefs.getInstance(context).isFollowingTeam(team.getSlug());

        final String channelId = isFollowingTeam ? team.getSlug() : Const.NOTIFY_DEFAULT_CHANNEL_ID;
        final String channelName = context.getString(isFollowingTeam ? team.getTeamName() :
                R.string.notify_default_channel);

        return new NotificationChannel(
                channelId,
                channelName,
                NotificationManager.IMPORTANCE_DEFAULT);
    }


    @TargetApi(Build.VERSION_CODES.O)
    public static void editAppNotificationsSettings(@NonNull Context context) {
        if (Const.SUPPORTS_OREO) {
            final Intent intent = new Intent(Const.IntentNames.APP_NOTIFICATION_SETTINGS);

            final Bundle extras = new Bundle();
            extras.putString(Settings.EXTRA_APP_PACKAGE, context.getPackageName());

            intent.putExtras(extras);

            context.startActivity(intent);
        }
    }

    public static boolean areNotificationsEnabled(Context context) {
        return NotificationManagerCompat.from(context).areNotificationsEnabled();
    }
}
