/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.utils;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.app.LoaderManager;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class LoaderUtils {
    private static final String TAG = makeLogTag("LoaderUtils");

    public static <D> void restartLoaderOnMainThread(final LoaderManager loaderManager,
                                                     final int id,
                                                     @Nullable final Bundle args,
                                                     @NonNull final LoaderManager.LoaderCallbacks<D> callback) {
        final Looper mainLooper = Looper.getMainLooper();
        if (Looper.myLooper() == mainLooper) {
            try {
                loaderManager.restartLoader(id, args, callback);
            } catch (Exception e) {
                LogUtils.REMOTE_LOG(e);
            }
        } else {
            new Handler(mainLooper).post(new Runnable() {
                @Override
                public void run() {
                    try {
                        loaderManager.restartLoader(id, args, callback);
                    } catch (Exception e) {
                        LogUtils.REMOTE_LOG(e);
                    }
                }
            });
        }
    }
}
