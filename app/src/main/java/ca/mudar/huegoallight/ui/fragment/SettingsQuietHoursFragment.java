/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.Const.FragmentTags;
import ca.mudar.huegoallight.Const.PrefsValues;
import ca.mudar.huegoallight.Const.TimePeriodFields;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.ui.dialog.TimePickerFragment;
import ca.mudar.huegoallight.utils.TimeUtils;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class SettingsQuietHoursFragment extends PreferenceFragment implements
        Const.PrefsNames,
        SharedPreferences.OnSharedPreferenceChangeListener,
        Preference.OnPreferenceClickListener {
    private static final String TAG = makeLogTag("SettingsQuietHoursFragment");

    private Preference mStartTime;
    private Preference mEndTime;

    public static SettingsQuietHoursFragment newInstance() {
        return new SettingsQuietHoursFragment();
    }

    private SharedPreferences mSharedPrefs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final PreferenceManager pm = this.getPreferenceManager();
        pm.setSharedPreferencesName(Const.APP_PREFS_NAME);
        pm.setSharedPreferencesMode(Context.MODE_PRIVATE);

        addPreferencesFromResource(R.xml.prefs_quiet_hours);

        mStartTime = findPreference(QUIET_HOURS_START);
        mEndTime = findPreference(QUIET_HOURS_END);

        mSharedPrefs = pm.getSharedPreferences();

        findPreference(QUIET_HOURS_START).setOnPreferenceClickListener(this);
        findPreference(QUIET_HOURS_END).setOnPreferenceClickListener(this);

        mSharedPrefs.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        setupSummaries();
        toggleEnabled(mSharedPrefs.getBoolean(QUIET_HOURS_ENABLED, PrefsValues.DEFAULT_QUIET_HOURS_ENABLED));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mSharedPrefs.unregisterOnSharedPreferenceChangeListener(this);
    }

    /**
     * Implements OnSharedPreferenceChangeListener
     *
     * @param sharedPreferences
     * @param key
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (QUIET_HOURS_START.equals(key) || QUIET_HOURS_END.equals(key)) {
            if (QUIET_HOURS_START.equals(key)) {
                mStartTime.setSummary(TimeUtils.getTimeDisplay(getAppContext(),
                        sharedPreferences.getInt(QUIET_HOURS_START, PrefsValues.DEFAULT_QUIET_HOURS_START)));
            }

            // In both cases, we need to update endTime for the overnight suffix
            mEndTime.setSummary(getEndTimeSuffixIfNecessary(
                    TimeUtils.getTimeDisplay(getAppContext(),
                            sharedPreferences.getInt(QUIET_HOURS_END, PrefsValues.DEFAULT_QUIET_HOURS_END))));
        } else if (QUIET_HOURS_ENABLED.equals(key)) {
            toggleEnabled(sharedPreferences.getBoolean(QUIET_HOURS_ENABLED, PrefsValues.DEFAULT_QUIET_HOURS_ENABLED));
        }
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        final String key = preference.getKey();

        if (QUIET_HOURS_START.equals(key) || QUIET_HOURS_END.equals(key)) {
            final HuePrefs prefs = HuePrefs.getInstance(getAppContext());

            final TimePickerFragment dialog;
            if (QUIET_HOURS_START.equals(key)) {
                dialog = TimePickerFragment.newInstance(TimePeriodFields.START,
                        prefs.getQuietHoursStart());
            } else {
                dialog = TimePickerFragment.newInstance(TimePeriodFields.END,
                        prefs.getQuietHoursEnd());
            }
            dialog.show(getFragmentManager(), FragmentTags.TIME_PICKER);

            return true;
        }

        return false;
    }

    private Context getAppContext() {
        return getActivity().getApplicationContext();
    }

    private void toggleEnabled(boolean enabled) {
        mStartTime.setEnabled(enabled);
        mEndTime.setEnabled(enabled);
    }

    private void setupSummaries() {
        final Context context = getAppContext();
        final HuePrefs prefs = HuePrefs.getInstance(context);

        mStartTime.setSummary(TimeUtils.getTimeDisplay(context, prefs.getQuietHoursStart()));
        mEndTime.setSummary(getEndTimeSuffixIfNecessary(
                TimeUtils.getTimeDisplay(context, prefs.getQuietHoursEnd())));
    }

    public String getEndTimeSuffixIfNecessary(String endTime) {
        final boolean hasSuffix = HuePrefs.getInstance(getAppContext()).isQuietHoursOvernight();
        if (hasSuffix) {
            return getResources().getString(R.string.prefs_summary_quiet_hours_overnight,
                    endTime);
        }

        return endTime;
    }
}
