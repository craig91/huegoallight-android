/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.OvershootInterpolator;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.squareup.otto.Subscribe;

import androidx.annotation.NonNull;
import androidx.annotation.UiThread;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.HueGoalApp;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.model.NhlTeam;
import ca.mudar.huegoallight.ui.adapter.NhlTeamsAdapter;
import ca.mudar.huegoallight.ui.listener.SyncBusEvents;
import ca.mudar.huegoallight.ui.listener.SyncBusListener;
import ca.mudar.huegoallight.utils.ConnectionUtils;
import ca.mudar.huegoallight.utils.GcmUtils;
import ca.mudar.huegoallight.utils.HockeyUtils;
import ca.mudar.huegoallight.utils.LogUtils;

import static ca.mudar.huegoallight.utils.LogUtils.LOGE;
import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class TeamsListFragment extends Fragment implements
        NhlTeamsAdapter.OnTeamSelectedListener,
        SyncBusListener,
        OnSuccessListener<InstanceIdResult>,
        OnFailureListener {
    private static final String TAG = makeLogTag("TeamsListFragment");

    private static final long ANIM_DURATION = 750; // milli-seconds
    private static final int ANIM_DELAY = 1000; // milli-seconds

    private NhlTeamsAdapter.OnTeamSelectedListener mListener;
    private RecyclerView vRecycler;
    private ProgressBar vProgressBar;

    private boolean mIsFcmRegistered;
    private String mSelectedTeam;

    public static TeamsListFragment newInstance() {
        return new TeamsListFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (NhlTeamsAdapter.OnTeamSelectedListener) context;
        } catch (ClassCastException e) {
            LOGE(TAG, "Activity must implement OnTeamSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_teams_list, container, false);

        vProgressBar = (ProgressBar) view.findViewById(R.id.progress);
        vRecycler = (RecyclerView) view.findViewById(R.id.recycler);

        if (savedInstanceState != null) {
            mIsFcmRegistered = savedInstanceState.getBoolean(Const.BundleKeys.IS_FCM_REGISTERED, false);
            mSelectedTeam = savedInstanceState.getString(Const.BundleKeys.TEAM_SLUG, null);
        }

        setupRecyclerView(vRecycler);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        registerSyncBus();

        setupFcm(getActivity());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(Const.BundleKeys.IS_FCM_REGISTERED, mIsFcmRegistered);
        outState.putString(Const.BundleKeys.TEAM_SLUG, mSelectedTeam);
    }

    @Override
    public void onPause() {
        super.onPause();

        unregisterSyncBus();
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void registerSyncBus() {
        try {
            HueGoalApp.getSyncBus().register(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void unregisterSyncBus() {
        try {
            HueGoalApp.getSyncBus().unregister(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    /**
     * Implements FCM OnSuccessListener<InstanceIdResult>
     *
     * @param instanceIdResult
     */
    @Override
    public void onSuccess(InstanceIdResult instanceIdResult) {
        onFcmRegistrationResult(true);
    }

    /**
     * Implements FCM OnFailureListener
     *
     * @param e
     */
    @Override
    public void onFailure(@NonNull Exception e) {
        onFcmRegistrationResult(false);
    }

    @Subscribe
    public void onFcmRegistrationError(SyncBusEvents.FcmRegistrationError error) {
        onFcmRegistrationResult(false);
    }

    @Override
    public void onTeamSelected(String team) {
        mListener.onTeamSelected(team);

        mSelectedTeam = team;
        toggleProgressBar(true);
    }

    public void toggleProgressBar(boolean loading) {
        vProgressBar.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    private void onFcmRegistrationResult(final boolean isRegistered) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isRegistered && mIsFcmRegistered) {
                    // Teams have already been displayed, no need to replay the anim
                    toggleProgressBar(false);
                    vRecycler.setVisibility(View.VISIBLE);
                } else if (isRegistered) {
                    animateSlideInWhenShown(getView(), ANIM_DELAY);
                } else if (getView() != null) {
                    toggleProgressBar(false);

                    Snackbar.make(getView(),
                            R.string.snackbar_fcm_onboarding_error,
                            Snackbar.LENGTH_INDEFINITE
                    ).setAction(R.string.btn_ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            animateSlideInWhenShown(getView(), 0);
                        }
                    }).show();
                }

                mIsFcmRegistered = isRegistered;
            }
        });
    }

    private void setupRecyclerView(RecyclerView recycler) {
        recycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recycler.setItemAnimator(new DefaultItemAnimator());

        final NhlTeamsAdapter mAdapter = new NhlTeamsAdapter(getContext(), R.layout.item_team, this);
        if (!TextUtils.isEmpty(mSelectedTeam)) {
            // Load single Team (one previously selected)
            mAdapter.swapDataset(new NhlTeam(mSelectedTeam));
        } else {
            // Load all teams
            mAdapter.swapDataset(HockeyUtils.getNhlTeams());
        }

        recycler.setAdapter(mAdapter);
    }

    /**
     * Get Firebase InstanceId with onSuccess and onFailure listeners.
     *
     * @param activity
     */
    private void setupFcm(final Activity activity) {
        if (ConnectionUtils.hasConnection(activity)) {
            if (GcmUtils.checkPlayServices(activity)) {
                toggleProgressBar(true);

                FirebaseInstanceId.getInstance().getInstanceId()
                        .addOnSuccessListener(this)
                        .addOnFailureListener(this);
            }
        } else {
            toggleProgressBar(false);
            Snackbar.make(getView(),
                    R.string.snackbar_no_connection,
                    Snackbar.LENGTH_INDEFINITE
            ).setAction(R.string.snackbar_btn_retry, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setupFcm(activity);
                }
            }).show();
        }
    }

    @UiThread
    private void animateSlideInWhenShown(final View view, final int delay) {
        final int width = view.getWidth();
        if (width == 0) {
            // Early call, view is not rendered yet
            view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    animateSlideIn(width, delay);
                }
            });
        } else {
            // Late call, width is known
            animateSlideIn(width, delay);
        }
    }

    private void animateSlideIn(final int screenWidth, int delay) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    // Hide the progressBar
                    toggleProgressBar(false);

                    // Set initial values, transparent and at the screen right edge
                    vRecycler.setVisibility(View.VISIBLE);
                    vRecycler.setX(screenWidth);
                    vRecycler.setAlpha(0);

                    // Start SlideIn/FadeIn animation
                    vRecycler.animate()
                            .setInterpolator(new OvershootInterpolator())
                            .setDuration(ANIM_DURATION)
                            .x(0)
                            .alpha(1f)
                            .start();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }, delay);
    }
}
