/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.data;

import android.text.format.DateUtils;

import com.google.gson.Gson;
import com.philips.lighting.hue.sdk.utilities.impl.Color;

import java.util.ArrayList;
import java.util.List;

import ca.mudar.huegoallight.model.NhlTeam;

public class GcmDataWrapper {
    private String message;
    private String game;
    private String team;
    private List<int[]> colors;
    private long timestamp;

    public static GcmDataWrapper fromJson(String s) {
        return new Gson().fromJson(s, GcmDataWrapper.class);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public String getMessage() {
        return message;
    }

    public String getGame() {
        return game;
    }

    public String getTeam() {
        return team;
    }

    public long getTimestamp() {
        return timestamp * DateUtils.SECOND_IN_MILLIS;
    }

    public List<Integer> getColors() {
        final List<Integer> colorsList = new ArrayList<>();
        if (colors != null) {
            for (int[] color : colors) {
                colorsList.add(
                        // Hue
                        Color.rgb(color[0], color[1], color[2])
                );
            }
        }

        return colorsList;
    }

    public NhlTeam getNhlTeam() {
        return new NhlTeam(team, getColors().get(0));
    }
}
