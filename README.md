## Hockey Goal Light for Philips Hue

![Hue Goal Light][img_github]

### Make your Smart lights blink in celebration of your team’s goals
   
This forked version of the original Hue Goal Light provides a webhook feature that will trigger when a goal is scored. You can use this different ways, but my suggestion is to run a local webserver (e.g. apache, nginx) in your home on a supported device (e.g. Raspberry Pi, home server, etc.) that will receive the HTTP GET request and then trigger a local script to control the lights. This allows you to set your own customized goal routine for your Hue Lights or connect any other smart devices and trigger them using a supported API (e.g. TP Link Switches, TP Link plugs, Wemo Plugs, LIFX lights, etc) and make them flash as well.

**Suggested Tools to Control Lights via Python**<br>
TP-Link - https://github.com/GadgetReactor/pyHS100<br>
Philips Hue - https://github.com/quentinsf/qhue<br>
LIFX - https://github.com/mclarkk/lifxlan<br>

You can find example scripts in the scripts folder that you would host on your webserver. You will need some basic scripting or programming knowledge to configure this.
* php script to receive webhook request from the app and triggers local python or shell script
* python script to control your smart lights (hue, tp-link, wemo, lifx, etc.)

Please note that results are not real-time, usual delay is around 5-15 seconds if compared to live TV. There's less delay when streaming the game, or if you are able to pause/rewind your stream (or PVR). And when things are working too well, the app can be a spoiler!

## Legal Notice
This is a fan project and is not affiliated with, endorsed or sponsored by the NHL, the NHL teams or Philips Lighting B.V.

NHL and the NHL Shield are registered trademarks of the National Hockey League. All NHL logos and marks and NHL team logos and marks depicted herein are the property of the NHL and the respective teams and may not be reproduced without the prior written consent of NHL Enterprises, L.P. © NHL. All Right Reserved.

Philips and Philips Hue are registered trademarks of Koninklijke Philips N.V. © Philips Lighting B.V. All rights reserved.

## Features
* Built for hockey fans who own a Philips Hue light.
* Follow Multiple NHL teams.
* Blinking Hue lights and device notification when a goal is scored. 
* Diehard fans have the option to ignore goals scored by the enemy!
* Support for Android [Daydream][link_support_daydream].

**Important:** If you don't have a Philips Hue bridge, please do not try to install this app. You will be disappointed as it doesn't do much without Hue lights!

## Links

* [Website][link_huegoallight_website]
* [Privacy policy][link_huegoallight_privacy]
* [Hue Goal Light on Google Play][link_huegoallight_playstore]

## Credits

* Developed by [Mudar Noufal][link_mudar_ca] &lt;<mn@mudar.ca>&gt;
* Forked by [Craig Trought] &lt;<craig91@live.ca>&gt;

The Android app includes (thanks!) libraries and derivative work of the following projects:

* [Hue SDK](https://github.com/PhilipsHue/PhilipsHueSDK-Java-MultiPlatform-Android): &copy; Philips Lighting B.V.
* [Otto](http://square.github.io/otto/): &copy; Square Inc.
* [Retrofit](http://square.github.io/retrofit/): retrofit2, adapter-rxjava, converter-gson. &copy; Square Inc.
* [OkHttp](http://square.github.io/okhttp/): okhttp3, logging-interceptor. &copy; Square Inc.
* [Google Play Services](https://github.com/google/gson): Google Cloud Messaging (GCM). &copy; Google Inc.
* [Gson](https://github.com/google/gson): &copy; Google Inc.
* [Crashlytics SDK for Android](https://fabric.io/kits/android/crashlytics/summary): &copy; Google Inc.
* [Material Tap Target Prompt](https://github.com/sjwall/MaterialTapTargetPrompt): &copy; Samuel Wall.
* [DragListView](https://github.com/woxblom/DragListView): &copy; Magnus Woxblom.
* [AboutLibraries](https://github.com/mikepenz/AboutLibraries): &copy; Mike Penz.
* [Android Support Library](http://developer.android.com/tools/support-library/): appcompat, recyclerview, cardview, palette, design, annotations. &copy; AOSP.
* [AOSP](http://source.android.com/) &copy; The Android Open Source Project.
* [Android Asset Studio](http://romannurik.github.io/AndroidAssetStudio/): Icon generator. &copy; Roman Nurik.

These libraries are all released under the [Apache License v2.0][link_apache], except for Hue SDK and Crashlytics.

## Code license

    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

[![Android app on Google Play][img_devices]][link_huegoallight_playstore]

[link_huegoallight_playstore]: http://play.google.com/store/apps/details?id=ca.mudar.huegoallight
[link_huegoallight_website]: http://huegoallight.mudar.ca/
[link_huegoallight_privacy]: http://huegoallight.mudar.ca/privacy.html
[link_mudar_ca]: http://www.mudar.ca/
[link_gpl]: http://www.gnu.org/licenses/gpl.html
[link_apache]: http://www.apache.org/licenses/LICENSE-2.0
[link_support_daydream]: https://support.google.com/nexus/answer/2818748
[img_github]: http://huegoallight.mudar.ca/assets/img/huegoallight_github.png
[img_devices]: http://huegoallight.mudar.ca/assets/img/huegoallight_devices.png
[img_playstore_badge]: http://huegoallight.mudar.ca/assets/img/en_app_rgb_wo_60.png
